
var object = {
    one: 1,
    two: 2,
    three: {
            10: "ten",
            11: "eleven"
            },
    four: 4
};


var array = [
    {one: 1,
     two: 2,
     three:{10: "ten",
            11: "eleven"
            },
    },
    {12: "twelve",
     13: "thirteen"},
     "string"
];


var asd = {
    dsfyuasudf: "asd7sdf",
    aqwe: {
        asdasd: [
            'asd0', 'qqqq', {a: "b"},
        ],
    },
    qqq: [],

};

var bla = {
    dsfyuasudf: "asd7sdf",
    aqwe: {
        asdasd: [
            'asd0', 'qqqq', {a: "c"},
        ],
    },
    qqq: [],

};

function isNumbers(item) {
    return ((isFinite(item)) && !(typeof item === "string") );
}
function isString(item) {
    return (typeof item === "string");
}
function isUndefined(item) {
    return (typeof item === "undefined");
}
function isNull(item) {
    return (typeof item === null);
}
function isBoolean(item) {
    return (typeof item === "boolean");
}
function isFunction(item) {
    return (typeof item === "function");
}
function isArray(item) {
    return Array.isArray(item);
}
function isObject(item) {
    if (typeof item === null){
        return false;
    }else return ((item instanceof Object) && !(Array.isArray(item)))
}


function copyValue(item){
    // var a = {};
    //
    // for (var i in item) {
    //     a[i] = item[i];
    // }
    //
    // return a;
    var copy;

    if (isNumbers(item)||isString(item)||isBoolean(item)||isNull(item)||isUndefined(item)||isFunction(item)){
        copy = item;
        return copy;
    }

    if (isArray(item)){
       copy = [];
        item.forEach(function (value, i, arr) {
            copy[i] = copyValue(arr[i]);
        });
        return copy;
    }

    if (isObject(item)){
        copy = {};
        for (var key in item){
            copy[key] = copyValue(item[key])
        }
        return copy;
    }

}


function compareValue(itemOne, itemTwo) {

    if ((isNumbers(itemOne) && isNumbers(itemTwo)) || (isString(itemOne) && isString(itemTwo))
        || (isBoolean(itemOne) && isBoolean(itemTwo)) || (isNull(itemOne) && isNull(itemTwo))) {
        return (itemTwo === itemOne);
    }

    if (isArray(itemOne) && isArray(itemTwo) && (itemOne.length === itemTwo.length)) {
        var a = [];
        for (var i = 0;i<itemOne.length;i++){
            a = compareValue(itemOne[i], itemTwo[i]);
            if (a === false){
                return false
            }
        } return true
    }

    if (isObject(itemOne) && isObject(itemTwo)) {
        var b = {};
        for (var i in itemOne) {
            b = compareValue(itemOne[i], itemTwo[i]);
            if (b === false) {
                return false
            }
        }return true
    }
    return false
}

function parsingObj(obj) {
   var valueOne = [];
   var valueTwo = [];
   if (isObject(obj)){
        for (var i in obj){
            valueOne.push(i);
            if (isObject(obj[i])){
                valueTwo.push("Object: '" + i + "'");
                var ins = [];
                ins.push(parsingObj(obj[i]));
            } else
            valueTwo.push(obj[i])
        } return  ("key: " + valueOne + " | value: " + valueTwo + " | key-length: " + valueOne.length + " | value-length: "
     + valueTwo.length + " | insert-obj: "  + ins);
    }
}


console.log(parsingObj(object));

function sequence(start, step){
    var a;
    var b;
    if (isNumbers(start)&&isNumbers(step)){
        a = start;
        b = step;
      } else {
      a = 0;
      b = 1;
  } return function () {
        return (a += b)
    }

}

let generator = sequence(10, 8);
let generator1 = sequence(5, 2)
// console.log(generator());
// console.log(generator());
// console.log(generator());
//
// console.log(generator1());
// console.log(generator1());
// console.log(generator1());


function take(gen, x){
    var arr = [];
    for (var i=0; i<x; i++){
        arr[i] = gen()
    }
    return arr
}

console.log(take(generator, 5));


function add(a, b){
    return (a + b)
}

function mult(a, b, c, d) {
    return (a * b * c * d);
}



function partial(func, ...arr) {
    return function (...insArr) {
        return func.apply(func, arr.concat(insArr))
    }
}

var add5 = partial(add, 5);
// console.log(add5(2)); // 7
// console.log(add5(10)); // 15
// console.log(add5(8)); // 13

var mult23 = partial(mult, 2, 3);
// console.log(mult23(4, 5)); // 2*3*4*5 = 120
// console.log(mult23(1, 1)); // 2*3*1*1 = 6



// function bindContext(func, context) {
//     return function (){
//         return func.apply(context, arguments);
//         };
// }
//
// let bindContext = function(func, context, ...arg) {
//     return function() {
//         var a = Object.assign({}, context, arg);
//         return func.apply(a);
//     }
// };
//
// var man = {
//     age: 25,
//     getAge: function() {
//         return this.age;
//     }
// };
//
// console.log(bindContext(man.getAge, man, {age: 31}))
//
// console.log(bindContext(man.getAge, man, {age: 31}));

// let f = function() {
//     return alert("huray!!!")
// };
// var a = 1;
//
// var bindContext = function() {
//     return function () {
//         return
//     }
// };
//
// let man = {
//     age: 25,
//     getAge: function() {
//         return this.age
//     },
// };
//
// console.log(man.getAge());
// //
// // console.log(man.getAge.f());
//
// man.a;
//
// console.log(man.a);





var tree1 = {
    value: 1,
    next: [
        {
            value: 3,
            next: null
        },
        {
            value: 2,
            next: null
        }
    ]
};

var tree = {
    value: 3,
    next: [{
        value: 1,
        next: null
    },
        {
            value: 3,
            next: null
        },
        {
            value: 2,
            next: null
        },
        {
            value: 2,
            next: [
                {
                    value: 1,
                    next: null
                },
                {
                    value: 5,
                    next: null
                }
            ]
        }]
};



Object.prototype.Get = function (){
    var a = this;
    var sum = 0;

    if (!(typeof a === null) && ((a instanceof Object) && !(Array.isArray(a)))) {
        for (var i in a){
            if (typeof a[i] === "number"){
                sum += a[i]
            }
            if (typeof a[i] === null){
                sum += 0
            }
            if (Array.isArray(a[i])){
                var arr = a[i];
                for (var j = 0;j < arr.length; j++){
                    if (!(typeof arr[j] === null) && ((arr[j] instanceof Object) && !(Array.isArray(arr[j])))){
                         sum += arr[j].Get()
                    } else sum += arr[j]
                }
            }
        }
    } return sum
};


console.log(tree.Get());

console.log(tree1.Get());


var arr = [{date: '10.01.2017'}, {date: '05.11.2016'}, {date: '21.13.2002'}];


var sortArr = function (arr) {
    arr.sort(function (a, b) {
        var first = a.date.split(".");
        var second = b.date.split(".");

        var firstDate = new Date(+first[2], +first[1] - 1, +first[0]);
        var secondDate = new Date(+second[2], +second[1] - 1, +second[0]);

        if (firstDate > secondDate) return 1;
        if (firstDate < secondDate) return -1;

    });
    return arr
};

console.log(sortArr(arr));